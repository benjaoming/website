window.onload = function(e) {
    var details = document.querySelectorAll("section.service-details");
    for (var detail of details) {
        var toggle = document.createElement("a");
        var service_name = detail.previousElementSibling.querySelector("a").text;
        toggle.text = "Læs mere om " + service_name;
        toggle.onclick = function(e) {
            e.preventDefault();
            var service_name = e.target.previousElementSibling.previousElementSibling.querySelector("a").text;
            var hide = e.target.previousElementSibling.classList.toggle("hidden");
            e.target.text = (hide ? "Læs mere om " : "Skjul detaljer om ") + service_name;
        }
        detail.insertAdjacentElement('afterend', toggle);
        detail.classList.add("hidden");
    }
}
