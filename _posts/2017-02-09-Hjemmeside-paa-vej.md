---
title: Første spadestik
layout: post
---
# Så er vi i gang!

Vi er i gang med, at opbygge en hjemmeside for foreningen. Indtil videre kan du følge med
i foreningens arbejde på vores [GitLab server](https://git.data.coop/data.coop/pm). Har du 
lyst til at blive medlem og være med til at opbygge vores datakollektiv så vil du snarest 
kunne finde informationer om, hvordan og hvorledes her på siden.